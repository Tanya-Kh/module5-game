﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Program
    {
        static void Main()
        {
            Program program = new Program();
            Console.Clear();
            Console.Write("Welcome to the Game! Use left, right, up, down arrows for playing! \n Press any key to continue...");
            Console.ReadKey();
            program.Boards();
            Console.SetCursorPosition(2,2);
            Console.Write("O");
            Console.CursorVisible = false;
            int coordinateX = 2;
            int coordinateY = 2;
            do
            {
                program.Movement(ref coordinateX, ref coordinateY);
            } while (true);
        }
        
        const int width = 10;
        const int height = 10;
        const int bombsNumber = 10;
        int healthPoint = 10;
        int previousX = 2;
        int previousY = 2;
        readonly List<int[]> bombList = BombCreating();

        private void Movement(ref int coordinateX, ref int coordinateY)
        {
            ConsoleKeyInfo keyInfo = Console.ReadKey(true);
            if (keyInfo.Key == ConsoleKey.UpArrow || keyInfo.Key == ConsoleKey.DownArrow
                || keyInfo.Key == ConsoleKey.LeftArrow || keyInfo.Key == ConsoleKey.RightArrow)
            {
                Console.SetCursorPosition(coordinateX, coordinateY);
                Console.Write(" ");
                
                switch (keyInfo.Key)
                {
                    case ConsoleKey.LeftArrow:
                        coordinateX--;
                        break;
                    case ConsoleKey.UpArrow:
                        coordinateY--;
                        break;
                    case ConsoleKey.RightArrow:
                        coordinateX++;
                        break;
                    case ConsoleKey.DownArrow:
                        coordinateY++;
                        break;
                }

                if (coordinateX < 2 || coordinateY < 2 || coordinateX > 11 || coordinateY > 11)
                {
                    coordinateX = previousX;
                    coordinateY = previousY;
                }else if (coordinateX == 11 && coordinateY == 11)
                {
                    Console.Clear();
                    Console.Write("You won! \n Press any key to continue...");
                    Console.ReadKey();
                    Main();
                }
                else
                {
                    previousY = coordinateY;
                    previousX = coordinateX;
                }

                bool damage = false;

                if (Lost(coordinateX, coordinateY, ref damage))
                {
                    Console.Clear();
                    Console.Write("You have lost! \n Press any key to continue...");
                    Console.ReadKey();
                    Main();

                }
                else
                {
                    if (damage)
                    {
                        Console.Clear();
                        Console.Write("Bomb!!! But You still have " + healthPoint + " out of 10 points! \n Press any key to continue... ");
                        Console.ReadKey();
                        Boards();
                    }

                    Console.SetCursorPosition(coordinateX, coordinateY);
                    Console.Write("O");
                }
            }
        }

        private bool Lost(int x, int y, ref bool damage)
        {
            bool lost = false;
            for (int i = 0; i < bombList.Count; i++)
            {
                if (bombList[i][0] == x && bombList[i][1] == y)
                {
                    healthPoint -= bombList[i][2];
                    damage = true;
                }
            }

            if (healthPoint <= 0)
            {
                lost = true;
                
            }

            return lost;
        }

        private static List<int[]> BombCreating()
        {
            Random rnd = new Random();
            List<int[]> bombs = new List<int[]>();
            for (int i = 0; i < bombsNumber; i++)
            {
                int[] array = { rnd.Next(3, 10), rnd.Next(3, 10), rnd.Next(1, 10) };
                bombs.Add(array);
            }
            
            return bombs;
        }

        private void Boards()
        {
            Console.Clear();
            for (int i = 1; i <= (width + 2); i++)
            {
                Console.SetCursorPosition(i, 1);
                Console.Write("*");
            }
            for (int i = 1; i <= (width + 2); i++)
            {
                Console.SetCursorPosition(i, height + 2);
                Console.Write("*");
            }
            for (int i = 1; i <= (height + 1); i++)
            {
                Console.SetCursorPosition(1, i);
                Console.Write("*");
            }
            for (int i = 1; i <= (height + 1); i++)
            {
                Console.SetCursorPosition(width + 2, i);
                Console.Write("*");
            }
        }
    }
}
